﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;


namespace Ports
{
    [Serializable]
    class Ports
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string PortCode { get; set; }
        public string UnctadPortCode { get; set; }
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Url { get; set; }
        public string MainPortCode { get; set; }
    }
}
