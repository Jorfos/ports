﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Reflection;

namespace Ports
{
	class Program
	{
		public static Boolean programRunning = true;
		public static List<Ports> portList = new List<Ports>();

		static void Main(string[] args)
		{
			while (programRunning)
			{
				WhatDoesTheUserWantToDo();
			}
		}

		public static void WhatDoesTheUserWantToDo()
		{
			Console.WriteLine("");
			Console.WriteLine("What would you like to do? (insert number)");
			Console.WriteLine("(1) Load existing Ports");
			Console.WriteLine("(2) Import Ports from another file (Json format)");
			Console.WriteLine("(3) Search for a Port (only already loaded Ports)");
			Console.WriteLine("(4) Add Port");
			Console.WriteLine("(0) End Program");
			EnterNumber(Console.ReadLine());
		}

		public static void EnterNumber(string number)
		{
			switch (number)
			{
				case "0":
					programRunning = false;
					break;

				case "1":
					LoadExistingPorts();
					break;

				case "2":
					ImportPorts();
					break;

				case "3":
					SearchForPort();
					break;
				
				case "4":
					AddPort(); 
					break;

				case "8":
					portList = JsonConvert.DeserializeObject<List<Ports>>(File.ReadAllText(@"C:\Users\Jorrit\Desktop\Ports.json"));
					Console.WriteLine("Ports loaded from scratch!");
					break;

				default:
					Console.WriteLine("Something went wrong - Please enter a number");
					EnterNumber(Console.ReadLine());
					break;
			}
		}

		public static void LoadExistingPorts()
		{
			portList = JsonConvert.DeserializeObject<List<Ports>>(File.ReadAllText(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Ports.json")));
			Console.WriteLine("Ports loaded!");		   
		}

		public static void ImportPorts()
		{
			Console.WriteLine("Please enter the filepath:");
			string filepath = Console.ReadLine();

			if (File.Exists(filepath))
			{
				dynamic importedPorts = JsonConvert.DeserializeObject<List<Ports>>(File.ReadAllText(@filepath));

				foreach (Ports port in importedPorts)
				{
					portList.Add(port);
				}
				Console.WriteLine("Ports imported!");
			}
			else
			{
				Console.WriteLine("Something went wrong - Enter correct filepath!");
				ImportPorts();
			}
		}

		public static void SearchForPort()
		{
			List<Ports> foundPorts = new List<Ports>();
			Console.WriteLine("Please enter a Port name, code or ID:");
			string search = Console.ReadLine();

			foreach (Ports port in portList)
			{
				if (port.Name == search | port.PortCode == search | port.ID == search)
				{
					foundPorts.Add(port);
				}
			}
			Console.WriteLine(foundPorts.Count + " Ports found");
			
			foreach (Ports foundPort in foundPorts)
			{
				PrintPortInformation(foundPort);
				WhatToDoWithPort(foundPort);
				if(foundPort.PortCode != foundPort.MainPortCode)
				{
					foreach (Ports mainPort in portList)
					{
						if(mainPort.PortCode == foundPort.MainPortCode)
						{
							Console.WriteLine("Main Port information:");
							PrintPortInformation(mainPort);
							WhatToDoWithPort(mainPort);
						}
					}
				}			
			}
		}
		
		public static void WhatToDoWithPort(Ports port)
        {
			Console.WriteLine();
			Console.WriteLine("What would you like to do? (insert number)");
			Console.WriteLine("(1) See Port position on Goolge Maps?");
			Console.WriteLine("(2) Delete Port");
			Console.WriteLine("(3) Nothing");	

			switch (Console.ReadLine())
				{
					case "1":
						OpenPositionGoogleMaps(port.Latitude, port.Longitude);
						break;

					case "2":
						DeletePort(port.ID);
						break;

					case "3":
						break;

					default:
						Console.WriteLine("Something went wrong - Please enter a number");
						WhatToDoWithPort(port);
						break;
				}
        }

		public static void DeletePort(string str)
        {
			for(int i=0; i < portList.Count; i++)
            {
				if(portList[i].ID == str)
                {
					portList.RemoveAt(i);
					Console.WriteLine("Port removed!");
					SaveToDatabase();
                }
            }
        }

		public static void PrintPortInformation(Ports port)
		{
			Console.WriteLine("");
			Console.WriteLine("ID: " + port.ID);
			Console.WriteLine("Name: " + port.Name);
			Console.WriteLine("PortCode: " + port.PortCode);
			Console.WriteLine("UnctadPortCode: " + port.UnctadPortCode);
			Console.WriteLine("Country: " + port.Country);
			Console.WriteLine("Latitude: " + port.Latitude);
			Console.WriteLine("Longitude: " + port.Longitude);
			Console.WriteLine("Url: " + port.Url);
			Console.WriteLine("MainPortCode: " + port.MainPortCode);
		}

		public static void OpenPositionGoogleMaps(string la, string lo)
		{
			System.Diagnostics.Process.Start("http://google.com/maps/place/" + ConverterKommaPunkt(la) + "+" + ConverterKommaPunkt(lo));
		}

		public static string ConverterKommaPunkt(string str)
		{
			StringBuilder sb = new StringBuilder(str);
			for(int i= 0; i<sb.Length;i++)
			{
				if(sb[i] == ',')
				{
					sb[i] = '.';
				}
			}
			return sb.ToString();
		}

		public static void AddPort()
        {
			Console.WriteLine("Welcome to AddPort - Enter the data");
			Ports newPort = new Ports();

			Console.WriteLine("");
			Console.WriteLine("Enter Port ID");
			newPort.ID = Console.ReadLine();
			Console.WriteLine("Enter Port Name");
			newPort.Name = Console.ReadLine();
			Console.WriteLine("Enter Portal Code");
			newPort.PortCode = Console.ReadLine();
			Console.WriteLine("Enter Unctad Port Code");
			newPort.UnctadPortCode = Console.ReadLine();
			Console.WriteLine("Enter Country");
			newPort.Country = Console.ReadLine();
			Console.WriteLine("Enter Latitude");
			newPort.Latitude = Console.ReadLine();
			Console.WriteLine("Enter Longitude");
			newPort.Longitude = Console.ReadLine();
			Console.WriteLine("Enter URL");
			newPort.Url = Console.ReadLine();
			Console.WriteLine("Enter MainPortCode");
			newPort.MainPortCode = Console.ReadLine();

			PrintPortInformation(newPort);
			portList.Add(newPort);
			Console.WriteLine("");
			Console.WriteLine("Port added!");
			SaveToDatabase();
        }

		public static void SaveToDatabase()
        {
			string json = JsonConvert.SerializeObject(portList.ToArray(), Formatting.Indented);
			System.IO.File.WriteAllText(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Ports.json"), json);
			Console.WriteLine("Database saved!");
        }
	}
}
